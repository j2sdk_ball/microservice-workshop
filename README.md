# microservice-workshop

# Start

To start services run the command:

```
$ docker-compose up -d
```

# SonarQube

Go to http://localhost:9000 to access SonarQube and login with:

- username: admin
- password: admin
