project = 'web'
branch = 'develop'
dockerUrl = 'j2sdkball/'
dockerImage = 'node:10.12.0-alpine'
httpProxy = ''
noProxy = ''
dockerArgs = '-u root:root'
version = ''

pipeline {
    // when running on single docker container, 
    // agent { label 'master' } = agent any = docker host (local machine)
    agent {
		label 'master'
    }

    // triggers {
    //     pollSCM('H/2 * * * *')
    // }

    // tools {
    //     jdk 'jdk8'
    // }

    options {
        skipDefaultCheckout true
    }

    stages {
        stage('Lint Check') {
            agent {
                docker {
                    image "${dockerImage}"
                    args  "${dockerArgs}"
                    reuseNode true
                }
            }
            steps {
                // hidden stage: clenup workspace and pull source
                deleteDir()
                checkout scm

                dir('web') {
                    sh 'npm i -g yarn@1.12.3'
                    sh 'yarn'
                    sh 'yarn lint'
                }
            }
        }

        stage('Unit Test') {
            agent {
                docker {
                    image "${dockerImage}"
                    args  "${dockerArgs}"
                    reuseNode true
                }
            }
            steps {
                dir('web') {
                    sh 'npm i -g yarn@1.12.3'
                    sh 'yarn'
                    sh 'yarn test:unit'
                }
            }
        }

        stage('Dependency Check') {
            agent {
                docker {
                    image "${dockerImage}"
                    args  "${dockerArgs}"
                    reuseNode true
                }
            }
            steps {
                dir('web'){
                    sh 'npm i -g yarn@1.12.3'
                }
                
                // yarn audit support since yarn@1.12.0 with optional yarn.lock but runs faster
                script {
                    sh(script: "cd web/ && (yarn audit | grep critical) && exit 2 || exit 0", returnStdout: true)
                }
            }
        }

        stage('Code Analysis') {
            environment {
                // scannerHome: /var/jenkins_home/tools/hudson.plugins.sonar.SonarRunnerInstallation/SonarQubeScanner
                scannerHome = tool 'SonarQubeScanner'
            }
            steps {
                sh 'node --version'
                sh 'npm --version'

                withSonarQubeEnv('sonarqube') {
                    dir('web') {
                        sh """
                            ${scannerHome}/bin/sonar-scanner \
                            -Dsonar.projectKey=microservice-workshop.web.demo \
                            -Dsonar.projectName=microservice-workshop.web.demo \
                            -Dsonar.projectVersion=0.1 \
                            -Dsonar.projectDescription='Web Demo for Microservice Workshop' \
                            -Dsonar.sources=./src \
                            -Dsonar.language=ts \
                            -Dsonar.profile='Sonar way' \
                            -Dsonar.dynamicAnalysis=reuseReports \
                            -Dsonar.sourceEncoding=UTF-8 \
                            -Dsonar.exclusions=.jenkins/**,dist/**,env/**,node_modules/**/*
                        """
                    }
                }
            }
        }

        stage('Quality Gate') {
            steps {
                timeout(time: 10, unit: 'MINUTES') {
                    waitForQualityGate abortPipeline: true
                }   
            }
        }

        stage('Git Tag') {
            agent {
                docker {
                    image "${dockerImage}"
                    args  "${dockerArgs}"
                    reuseNode true
                }
            }
            steps {
                echo "workspace: ${WORKSPACE}"
                
                sh 'apk update && apk add --no-cache bash git openssh'
                
                sh 'git remote remove origin'
                sh """git remote add origin https://${GIT_USERNAME}:${GIT_PASSWORD}@github.com/j2sdkball/microservice-workshop.git""" 
                sh 'git fetch'
                sh """git checkout ${branch}"""
                sh """git pull origin ${branch}"""

                script {
                    version = sh(script: "cd web/ && npm version prerelease --preid=${VERSION_META} | tr -d '\n'", returnStdout: true)
                    
                    echo "version: ${version}"
                }
                
                dir('web') {
                    // sh 'cat package.json'
                    
                    sh 'git status'
                    sh 'git log -n 1'
                    sh 'git tag'
                    
                    sh 'git add package.json'
                    
                    sh 'git config --global user.email "1203ball@gmail.com"'
                    sh 'git config --global user.name "j2sdkball"'

                    sh """git commit -m 'Update version ${version}'""" 
    
                    sh 'git status'
                    sh 'git log -n 1'
                    
                    sh """git tag -a ${version} -m 'Tag from jenkins ${version}'"""
                    sh """git push origin ${branch}""" 
                    sh """git push origin ${version}"""
                }
            }
        }

        stage('Build Artifact') {
            steps {
                dir('web'){
                    sh "docker build -t demo:${version} ."
                    sh "docker tag demo:${version} ${dockerUrl}demo:${version}"
                }
            }
        }

        stage('Push Artifact') {
            steps {
                sh "docker login -u ${DOCKER_USERNAME} -p ${DOCKER_PASSWORD}"
                sh "docker push ${dockerUrl}demo:${version}"
            }
        }

        stage('Deploy') {
            steps {
                echo 'done'
            }
        }
    }
}