const channelState = {
  loading: false,
  message: 'Loading message from server...',
  error: '',
}

export default (state = channelState, action: any) => {
  const { type, ...rest } = action

  if (type.startsWith('helloWorld/')) {
    return Object.assign({}, state, rest)
  }

  return state
}
