export interface IHelloWorldAction {
  type: string
  loading: boolean
}

export const LOAD = 'helloWorld/load'
export const load = (): IHelloWorldAction => ({
  type: LOAD,
  loading: true,
})

export const DONE = 'helloWorld/load/done'
export const done = (): IHelloWorldAction => ({
  type: DONE,
  loading: false,
})

export const CANCEL = 'helloWorld/load/cancel'
export const cancel = (): IHelloWorldAction => ({
  type: CANCEL,
  loading: false,
})
