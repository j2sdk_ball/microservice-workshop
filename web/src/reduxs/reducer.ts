import { combineReducers } from 'redux'

import helloWorld from './hello-world/reducer'
// import { IAppState } from '../common/state'

export default combineReducers({
  helloWorld,
})
