import * as React from 'react'
import { shallow } from 'enzyme'
import { Provider } from 'react-redux'
import { createEpicMiddleware } from 'redux-observable'
import configureStore from 'redux-mock-store'

import Index from './index'

test('Index load with message', () => {
  const epicMiddleware = createEpicMiddleware()
  const mockStore = configureStore([epicMiddleware])

  const store = mockStore(() => ({
    thing: 'a thing',
  }))

  const wrapper = shallow(
    <Provider store={store}>
      <Index message="" dispatch={() => ({})} />
    </Provider>,
  ).shallow()

  // Interaction demo
  expect(wrapper.find('#message').text()).toEqual('xxx')
  // expect(wrapper.text()).toEqual('Off')
  // checkbox.find('input').simulate('change')
  // expect(checkbox.text()).toEqual('On')

  // Snapshot demo
  expect(wrapper).toMatchSnapshot()
})
