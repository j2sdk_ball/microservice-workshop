import React, { Component, ReactNode } from 'react'
import { connect } from 'react-redux'
import { Icon } from 'antd'

import Layout from '../layout'
import { load, IHelloWorldAction } from '../reduxs/hello-world/action'
import { IAppState, IHelloWorldState } from '../common/state'
// import { Dispatch } from 'redux'

// interface IProps extends IHelloWorldState {
//   dispatch: Dispatch<IHelloWorldAction>
//   children?: ReactNode
// }

// @connect<IHelloWorldState, void>(
//   //
//   (state: IAppState) => state.helloWorld,
//   dispatch => ({ dispatch }),
// )
class Index extends Component<
  IHelloWorldState & {
    dispatch: (action: IHelloWorldAction) => any
  } /*{ dispatch: Dispatch<IHelloWorldAction> }*/ & {
    children?: ReactNode
  },
  // IProps,
  {}
> {
  componentDidMount() {
    const { dispatch } = this.props
    dispatch(load())
  }

  render() {
    const { message } = this.props

    return (
      <Layout>
        {/* {loading && <Icon type="loading-3-quarters" theme="outlined" />} */}
        <div className="flex">
          <div className="mr-2">
            <Icon type="loading" theme="outlined" spin />
          </div>

          <div id="message">{message}</div>
        </div>
      </Layout>
    )
  }
}

export default connect(
  (state: IAppState): IHelloWorldState => state.helloWorld,
  (dispatch): any => ({ dispatch }),
)(Index)
