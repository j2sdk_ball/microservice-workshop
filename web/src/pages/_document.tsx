import Document, { Head, Main, NextScript } from 'next/document'

const antdStyle = require('antd/dist/antd.min.css')
const tailwindStyle = require('../styles/styles.css')

export default class MyDocument extends Document {
  static async getInitialProps(ctx: any) {
    const initialProps = await Document.getInitialProps(ctx)
    return { ...initialProps }
  }

  render() {
    return (
      <html lang="en">
        <Head>
          <style dangerouslySetInnerHTML={{ __html: tailwindStyle }} />
          <style dangerouslySetInnerHTML={{ __html: antdStyle }} />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </html>
    )
  }
}
