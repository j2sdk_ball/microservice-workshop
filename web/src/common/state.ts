export interface IAppState {
  helloWorld: IHelloWorldState
}

export interface IHelloWorldState {
  message: string
}
