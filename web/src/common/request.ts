import axios from 'axios'

const headers: any = { 'Content-Type': 'application/json' }

export const getJson = (url: string): Promise<any> =>
  axios({
    url,
    method: 'GET',
    headers,
  })

export const postJson = (url: string, data: any = {}): Promise<any> =>
  axios({
    url,
    method: 'POST',
    headers,
    data,
  })
