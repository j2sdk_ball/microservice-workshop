import { createLogger, format, transports, Logger } from 'winston'
import fs from 'fs'

const { combine, timestamp, label, printf, colorize, json } = format

const myFormat = printf(info => {
  return `${info.timestamp} ${info.level}: ${info.message}`
})

const path = 'logs'

export const init = (): boolean => {
  if (!fs.existsSync(path)) {
    fs.mkdirSync(path)
    return true
  }

  return false
}

export const logger: Logger = createLogger({
  format: combine(timestamp(), colorize(), json()),
  transports: [
    // new UDP({
    //   server: 'fluentd',
    //   port: 5160,
    //   // level: 'debug',
    // }),
    new transports.File({
      filename: 'logs/error.log',
      level: 'error',
    }),
    new transports.File({
      filename: 'logs/combined.log',
    }),
  ],
})

if (process.env.NODE_ENV !== 'production') {
  logger.add(
    new transports.Console({
      format: combine(timestamp(), colorize(), json()),
    }),
  )
}
