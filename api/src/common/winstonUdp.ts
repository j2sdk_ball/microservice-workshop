import dgram from 'dgram'
import Transport from 'winston-transport'

let serverError = false
const options = {
  level: 'info',
  server: 'localhost',
  port: '5160',
}
const client = dgram.createSocket('udp4')

export default class UDP extends Transport {
  name: string

  constructor(opts = options) {
    for (const option in opts) {
      if (Object.prototype.hasOwnProperty.call(opts, option)) {
        options[option] = opts[option]
      }
    }
    super(options)
    this.name = 'fluentudp'
  }

  log(level, message, meta, callback) {
    const self = this

    if (this.formatter) {
      const formatterOptions = {}
      formatterOptions.level = level
      formatterOptions.meta = meta
      formatterOptions.timestamp = options.timestamp
      formatterOptions.message = message
      message = this.formatter(formatterOptions)
    }

    const msg = Object.assign({ level }, { message }, meta)
    message = Buffer.from(JSON.stringify(msg)).slice(0, 65507)
    if (!serverError) {
      try {
        client.send(
          message,
          0,
          message.length,
          options.port,
          options.server,
          err => {
            if (err) {
              serverError = err
              client.emit('error', err)
              self.emit('error', err)
              if (callback) {
                callback(err, false)
              }
            } else {
              self.emit('logged')
              if (callback) {
                callback(null, true)
              }
            }
          },
        )
      } catch (error) {
        serverError = error
        client.emit('error', error)
        self.emit('error', error)
        if (callback) {
          callback(error, false)
        }
      }
    }
  }

  closeClient() {
    if (client && typeof client.close === 'function') {
      client.close()
    }
  }
}
