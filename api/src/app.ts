import express, { Application } from 'express'
import cors from 'cors'
import bodyParser from 'body-parser'

import { init as initLog, logger } from './common/log'
import { name, version, description, author } from '../package.json'
import { port } from './config'

const app: Application = express()

app.use(cors())
app.use(bodyParser.json())

initLog()

app.get('/', (req, res) => res.json({ name, version, description, author }))

app.listen(port, () => {
  logger.info(`server started at http://localhost:${port}`)
})
